﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ZedGraph;

namespace DataAnalysis
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        //create close button
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close(); //closes current form
        }

        //create variables for splitting the data
        private string splitline;
        string[] splitTabs;
        string heartValue;
        string speedValue;
        string cadenceValue;
        string altitudeValue;
        string powerValue;
        string powerBalanceValue;

        // create 4 lists for ordering the data in the grid view
        List<int> iList = new List<int>();
        List<int> iList1 = new List<int>();
        List<int> iList2 = new List<int>();
        List<int> iList3 = new List<int>();

        private void Form1_Load(object sender, EventArgs e)
        {

            //create open file dialog 
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //locate data file
            openFileDialog1.FileName = @"C:\Users\Paddy\Documents\Software B\CycleData.hrm";

            //create string for located file
            string strfilename = openFileDialog1.FileName;

            //create array that reads file
            string[] filetext = File.ReadAllLines(strfilename);

            //format date input
            String[] formats = { "yyyy/MM/dd" };
            foreach (var format in formats) ;

            //create string arrays to split text up after '=' sign. output split up data to text boxes.
            string[] date = filetext[4].Split('=');
            textBox3.Text = ("Date = " + date[1]);

            string[] time = filetext[5].Split('=');
            textBox1.Text = ("Time = " + time[1]);

            string[] interval = filetext[7].Split('=');
            textBox2.Text = ("Interval = " + interval[1]);

            string[] length = filetext[6].Split('=');
            string[] length1 = length[1].Split(':');
            textBox8.Text = ("Length = " + length[1]);

            string[] vo2 = filetext[21].Split('=');
            textBox13.Text = ("VO2 Max = " + vo2[1] + " ml/min/kg");


            //create for loop to loop through file 
            for (int i = 0; i < filetext.Length; i++)
            {
                splitline = filetext[i];

                //create if statement to find correct line wihin file
                if (splitline.Contains("[HRData]"))
                {
                    // create for loop to loop through file
                     for (int a = (i + 1); a < filetext.Length; a++)
                    {
                        string split1 = filetext[a];

                        //split the string up 
                        splitTabs = split1.Split('\t');

                        //add values to the strings created ealrier
                        heartValue = splitTabs[0];
                        speedValue = splitTabs[1];

                        //convert strings to integers to perform calculations
                        int myinteger = Convert.ToInt32(splitTabs[1]);
                        int newint = myinteger / 10;

                        int myinteger1 = Convert.ToInt32(splitTabs[0]);
                        int newint1 = myinteger1;

                        int myinteger2 = Convert.ToInt32(splitTabs[3]);
                        int newint2 = myinteger2;

                        int myinteger3 = Convert.ToInt32(splitTabs[4]);
                        int newint3 = myinteger3;

                        //add values to the strings created ealrier
                        cadenceValue = splitTabs[2];
                        altitudeValue = splitTabs[3];
                        powerValue = splitTabs[4];
                        powerBalanceValue = splitTabs[5];

                        //create data grid view and assign number of columns
                        dataGridView1.ColumnCount = 6;

                        //name each column
                        dataGridView1.Columns[0].Name = "Heart Rate (BPM)";
                        dataGridView1.Columns[1].Name = "Speed (Km/h)";
                        dataGridView1.Columns[2].Name = "Cadence (RPM)";
                        dataGridView1.Columns[3].Name = "Altitude (m/ft)";
                        dataGridView1.Columns[4].Name = "Power(W)";
                        dataGridView1.Columns[5].Name = "Power Balance and pedalling index";

                        //add the split up data to the rows
                        dataGridView1.Rows.Add(splitTabs[0], newint, splitTabs[2], splitTabs[3], splitTabs[4], splitTabs[5]);

                        // add converted ints to the lists
                        iList.Add(newint);
                        iList1.Add(newint1);
                        iList2.Add(newint2);
                        iList3.Add(newint3);
                    }
                }

            }

            //average speed formula
            int sum1 = iList.Count; //count the number of lines
            int sum2 = iList.Sum(); //add all the values up
            int sum3 = sum2 / sum1; //divide them to fins the average
            string y = sum3.ToString(); //convert int to string
            int ll = 14;
            textBox4.Text = ("Average Speed = " + y + " km/h " + "(" + ll + "mph" + ")"); //output string into textbox

            //maximum speed formula
            int sss = iList.Max(); //find highest integer from the list
            int hh = 31;
            textBox15.Text = ("Maximum Speed = " + sss + " km/h " + "(" + hh + "mph" + ")");

            //maximum heart rate formula
            int ss = iList1.Max();
            textBox5.Text = ("Maximum Heart Rate = " + ss + " bpm");

            //mimimum heart rate formula
            int pp = iList1.Min(); //find the lowest integer from the list
            textBox7.Text = ("Minimum Heart Rate = " + pp + " bpm");

            //maximum alititude formula
            int aa = iList2.Max();
            textBox17.Text = ("Maximum Altitude = " + aa + " m/ft");

            //maximum power formula
            int mm = iList3.Max();
            textBox16.Text = ("Maximum Power = " + mm + "W");

            //average heart formula
            int sum4 = iList1.Count; 
            int sum5 = iList1.Sum();
            int sum6 = sum5 / sum4;
            string x = sum6.ToString();
            textBox10.Text = ("Average Heart Rate = " + x + " bpm");

            //average altitude formula
            int sum7 = iList2.Count;
            int sum8 = iList2.Sum();
            int sum9 = sum8 / sum7;
            string v = sum9.ToString();
            textBox11.Text = ("Average Altitude = " + v + " m/ft");

            //average power formula
            int sum10 = iList3.Count;
            int sum11 = iList3.Sum();
            int sum12 = sum11 / sum10;
            string b = sum12.ToString();
            textBox12.Text = ("Average Power = " + b + "W");

            //tryparse the string to return an integer 
            Int32.TryParse(length1[0], out int hours);
            Int32.TryParse(length1[1], out int t);

            //total distance covered formula
            int hours1 = hours * 60; //convert to minutes
            int to = hours1 += t; 
            int avgs = to * sum3; //times by average speed
            int avgs1 = avgs / 60; //divide by 60
            int avgs2 = 15;
            textBox14.Text = ("Total Distance Covered = " + avgs1 + "km" + "(" + avgs2 + " miles" + ")"); //output to textbox


        }

        //create button linking form to graph form
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide(); //hides the current form
            var graph = new graph(); //creates a new variable that is linked to the graph form
            graph.Show(); //displays the graph form
        }

    }


}