﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace DataAnalysis
{
    public partial class graph : Form
    {
        public graph()
        {
            InitializeComponent();
        }
        
        //create graph load class
         private void graph_Load(object sender, EventArgs e)
        {
            plotGraph();
            plotGraph1();
          
        }

        // create for loops to build data for the graphs
        private int[] buildTeamAData()
        {
            int[] goalsScored = new int[10];
            for (int i = 0; i < 10; i++)
            {
                goalsScored[i] = (i + 1) * 10;
            }
            return goalsScored;
        }

        //create the format of the graph and plot it 
        private void plotGraph()
        { GraphPane myPane = zedGraphControl1.GraphPane; //create a new graph pane

            myPane.Title = "Speed Graph"; //name of graph
            myPane.XAxis.Title = "Time(minutes)"; //x axis label
            myPane.YAxis.Title = "Speed(Km/h)";  //y axis label

            PointPairList teamAPairList = new PointPairList(); // create a point pair list
            
            int[] teamAData = buildTeamAData(); //create data int array
            
            for (int i = 0; i < 10; i++)
            {
                teamAPairList.Add(i, teamAData[i]);
              

                //create name for graph key and colour and shape. 
                LineItem teamACurve = myPane.AddCurve("Rider", teamAPairList, Color.Red, SymbolType.Diamond);
                zedGraphControl1.AxisChange();


        }
       

        }

        //create cadence graph
        private void plotGraph1()
        {
            //create second pane and label graph
            GraphPane myPane = zedGraphControl2.GraphPane;
            myPane.Title = "Cadence Graph";
            myPane.XAxis.Title = "Time(minutes)";
            myPane.YAxis.Title = "Cadence(RPM)";

            PointPairList teamAPairList = new PointPairList();      
            int[] teamAData = buildTeamAData();
            
            for (int i = 0; i < 10; i++)
            {
                teamAPairList.Add(i, teamAData[i]);
              

                LineItem teamACurve = myPane.AddCurve("Rider",
                  teamAPairList, Color.Blue, SymbolType.Square);
                zedGraphControl2.AxisChange();


            }
        }
          
        //create button to return to form1
            private void button1_Click(object sender, EventArgs e)
        {
            this.Hide(); //hides the current form
            var Form1= new Form1(); //creates a new variable that is linked to form1
            Form1.Show(); //displays form1
        }

        //create close button 
        private void button2_Click(object sender, EventArgs e)
        {

            this.Close(); //closes form
        }

    }
}
